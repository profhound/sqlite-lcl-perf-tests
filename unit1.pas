unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqlite3conn, sqldb, db, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DBGrids, ExtCtrls, DBCtrls, EditBtn, ActnList, ComCtrls, Menus;

type

  { TForm1 }

  TForm1 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RefreshMainMenu: TAction;
    MainMenu1: TMainMenu;
    RefreshComboBox: TAction;
    RefreshDBComboBox: TAction;
    SelectDates: TAction;
    GenerateRandomData: TAction;
    GenerateSchema: TAction;
    ActionList1: TActionList;
    ComboBox1: TComboBox;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DateEdit1: TDateEdit;
    DBComboBox1: TDBComboBox;
    DBGrid1: TDBGrid;
    DBLookupComboBox1: TDBLookupComboBox;
    LabeledEdit1: TLabeledEdit;
    memoRefreshOutput: TMemo;
    memoOutput: TMemo;
    MemoSelect: TMemo;
    memoSchema: TMemo;
    SQLite3Connection1: TSQLite3Connection;
    SQLQuery1: TSQLQuery;
    SQLQuery2: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    procedure GenerateRandomDataExecute(Sender: TObject);
    procedure GenerateSchemaExecute(Sender: TObject);
    procedure btnGenerateSchemaClick(Sender: TObject);
    procedure btnRandomDataClick(Sender: TObject);
    procedure btnSelectDatesClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RefreshComboBoxExecute(Sender: TObject);
    procedure RefreshDBComboBoxExecute(Sender: TObject);
    procedure RefreshMainMenuExecute(Sender: TObject);
    procedure SelectDatesExecute(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

uses DateUtils;

{ TForm1 }

procedure TForm1.btnGenerateSchemaClick(Sender: TObject);
begin

end;

procedure TForm1.GenerateSchemaExecute(Sender: TObject);
begin
  SQLQuery1.SQL.Clear;
  SQLQuery1.SQL.AddStrings(memoSchema.Lines);

  SQLite3Connection1.Connected:= False;
  SQLite3Connection1.DropDB;
  SQLite3Connection1.CreateDB;
  SQLQuery1.ExecSQL;
end;

procedure TForm1.GenerateRandomDataExecute(Sender: TObject);
var
  i, count : Integer;
  jDate: Double;
  julianString: String;
  available: Boolean;
  startTime, endTime: TDateTime;
begin
  startTime := Now;
  SQLQuery1.SQL.Clear;
  //SQLQuery1.SQL.text := 'INSERT INTO dates (julian_day, string, available ) VALUES (:julian_day, :string, :available)';
  SQLQuery1.SQL.text := 'INSERT INTO dates (julian_day, string, available ) VALUES (:julian_day, datetime(:julian_day), :available)';

  count:= StrToInt(LabeledEdit1.Text);
  for i := 1 to count do
  begin
     jDate := i;
     //julianString := FormatDateTime('YYYY-MM-DD hh:mm:ss.zzz', JulianDateToDateTime(jDate)); // TODO use sqlite function to calc this...
     available := not available;
     SQLQuery1.Params.ParamByName('julian_day').AsFloat:= jDate;
     //SQLQuery1.Params.ParamByName('string').AsString:= julianString;
     SQLQuery1.Params.ParamByName('available').AsBoolean:= available;
     SQLQuery1.ExecSQL;
  end;
  SQLQuery1.Close;
  endTime := Now;
  memoOutput.Lines.Clear;
  memoOutput.Lines.Text:= 'Data Generation Time Took: ' + IntToStr( MilliSecondsBetween(endTime, startTime) ) + ' (ms)';

end;

procedure TForm1.btnRandomDataClick(Sender: TObject);
begin
end;

procedure TForm1.btnSelectDatesClick(Sender: TObject);
begin


end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  DBGrid1.DataSource := nil;

  SQLQuery1.Close;
  SQLQuery1.SQL.Clear;
  SQLQuery1.PacketRecords:= StrToInt(LabeledEdit1.Text);
  SQLQuery1.SQL.AddStrings(memoSelect.Lines);
  SQLQuery1.Open;

  DBComboBox1.Refresh;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  //DateEdit1.Date:= -1000;
  RefreshMainMenu.Execute;
end;

procedure TForm1.RefreshComboBoxExecute(Sender: TObject);

var
  nCount,i :Integer;
  str: string;
  dataset: TDataSet;

  startTime, endTime: TDateTime;
  strings: TStringList;
  obj: TObject; // To test obj in TStringList
begin
  DBLookupComboBox1.ListSource := nil;
  DBLookupComboBox1.DataSource := nil;
  DBComboBox1.DataSource := nil;
  DBGrid1.DataSource := nil;

  startTime := Now;



  SQLQuery1.Close;
  SQLQuery1.SQL.Clear;
  SQLQuery1.PacketRecords:= -1;
  SQLQuery1.SQL.AddStrings(memoSelect.Lines);
  SQLQuery1.Open;

  dataset := SQLQuery1;//DataSource1.DataSet;

  nCount:=DataSet.RecordCount;
  DataSet.First;


  strings := TStringList.Create;
  strings.BeginUpdate;
  strings.Capacity:= nCount;
  While not (DataSet.Eof) do
  begin
     //Inc(i);
     str:= SQLQuery1.FieldByName('string').asString;
     //obj:= TObject.Create; // dummy for now, TODO how we gonna free this?
     //strings.AddObject(str, obj);
     strings.Add(str);
     DataSet.Next
  end;
  endTime := Now;
  memoRefreshOutput.Lines.Clear;
  memoRefreshOutput.Lines.Text:= 'StringList Time Took: ' + IntToStr( MilliSecondsBetween(endTime, startTime) ) + ' (ms)';
  startTime := Now;
  //ComboBox1.Items.BeginUpdate; // not needed to due to `ComboBox1.Items.Text := ...` using `TStrings.DoSetTextStr`
  //ComboBox1.Items.Clear; // not needed to due to `ComboBox1.Items.Text := ...` using `TStrings.DoSetTextStr`
  ComboBox1.Items.Capacity:= nCount;
  ComboBox1.Items.Text:= strings.Text;
  //ComboBox1.Items:= strings; // 5 times slower because it goes through TPersistent.Assign
  //ComboBox1.Items.EndUpdate;
  endTime := Now;
  memoRefreshOutput.Lines.Add('Update Time Took: ' + IntToStr( MilliSecondsBetween(endTime, startTime) ) + ' (ms)');
  strings.EndUpdate;

end;


procedure TForm1.RefreshDBComboBoxExecute(Sender: TObject);
begin

  DBGrid1.DataSource := nil;

  SQLQuery2.Close;
  SQLQuery2.SQL.Clear;
  SQLQuery2.SQL.AddStrings(memoSelect.Lines);
  SQLQuery2.Open;

  SQLQuery1.Close;
  SQLQuery1.SQL.Clear;
  //SQLQuery1.PacketRecords:= StrToInt(LabeledEdit1.Text);
  SQLQuery1.SQL.AddStrings(memoSelect.Lines);
  SQLQuery1.Open;


  DBLookupComboBox1.Refresh;
end;

procedure TForm1.RefreshMainMenuExecute(Sender: TObject);
var
  i: integer;
  a: TContainedAction;
  mi: TMenuItem;
begin
  MainMenu1.Items.Clear;
  for i := 0 to ActionList1.ActionCount - 1 do
  begin
    a := ActionList1.Actions[i];
    if a.Category = 'hidden' then
      continue;
    mi := TMenuItem.Create(MainMenu1);
    mi.Action := a;
    MainMenu1.Items.Add(mi);
  end;
end;

procedure TForm1.SelectDatesExecute(Sender: TObject);
begin
  DBLookupComboBox1.ListSource := nil;
  DBGrid1.DataSource := DataSource1;
  SQLQuery1.Close;
  SQLQuery1.SQL.Clear;
  SQLQuery1.PacketRecords:= StrToInt(LabeledEdit1.Text);
  SQLQuery1.SQL.AddStrings(memoSelect.Lines);
  SQLQuery1.Open;
  DBGrid1.Refresh;
end;


end.

